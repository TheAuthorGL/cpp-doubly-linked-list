cmake_minimum_required(VERSION 3.15)
project(ulacex_doubly_linked_list)

set(CMAKE_CXX_STANDARD 14)

add_executable(ulacex_doubly_linked_list main.cpp)