#include <iostream>

using namespace std;

/**
 * Data structure representing a node in a doubly linked list.
 */
struct node {
    int value;
    node* previous;
    node* next;
};

/**
 * Exception thrown when a node is requested from a linked list but does no exist.
 */
struct NodeUnavailableException: public exception {
    const char* what() const throw() {
        return "An unavailable or missing node was requested from a linked list.";
    }
};

/**
 * Implementation of a linked list where each node has a pointer to the next and previous node.
 */
class DoublyLinkedList {
private:
    node* head;
    node* tail;

public:
    /**
     * Gets the first item in the list.
     */
    node* getHead() {
        return head;
    }

    /**
     * Gets the last item in the list.
     */
    node * getTail() {
        return tail;
    }

    /**
     * Gets the current amount of items in the list.
     */
    int getLength() {
        if (head == NULL)
            return 0;

        node* current = head;
        int counter = 0;

        while (current != NULL) {
            counter++;
            current = current->next;
        }

        return counter;
    }

    /**
     * Gets the node at the given position.
     */
    node* getNode(int position) {
        if (head == NULL)
            throw NodeUnavailableException();

        node* current = head;
        int counter = 0;

        while (counter != position) {
            counter++;
            current = current->next;

            if (current == NULL)
                throw NodeUnavailableException();
        }

        return current;
    }

    /**
     * Gets the value of the node at the given position.
     */
    int getValue(int position) {
        return getNode(position)->value;
    }

    /**
     * Inserts a value at the start of the list.
     */
    void prepend(int value) {
        node* newHead = new node;

        newHead->value = value;
        newHead->next = head;
        newHead->previous = NULL;

        head = newHead;
        if (tail == NULL) {
            tail = newHead;
        }
    }

    /**
     * Inserts a value at the end of the list.
     */
    void append(int value) {
        node* newTail = new node;

        newTail->value = value;
        newTail->next = NULL;
        newTail->previous = tail;

        if (tail != NULL) {
            tail->next = newTail;
        }

        if (head == NULL) {
            head = newTail;
        }
        tail = newTail;
    }

    /**
     * Inserts a value at the given position of the list.
     */
    void insert(int value, int position) {
        node* newNode = new node;
        node* previousNode = getNode(position - 1);
        node* nextNode = getNode(position);

        newNode->value = value;
        newNode->previous = previousNode;
        newNode->next = nextNode;

        previousNode->next = newNode;
        nextNode->previous = newNode;
    }

    /**
     * Sorts the list in ascending order.
     *
     * @remark This uses the bubble sorting algorithm.
     */
    void sort() {
        if (head == NULL)
            return;

        bool swapped;
        do {
            swapped = false;

            node* current = head;

            while (current != NULL) {
                node* nextNode = current->next;

                if (nextNode != NULL && nextNode->value < current->value) {
                    int newValue = nextNode->value;
                    nextNode->value = current->value;
                    current->value = newValue;

                    swapped = true;
                }

                current = current->next;
            }
        } while (swapped);
    }
};

void displayDoublyLinkedList(DoublyLinkedList* list) {
    node* current = list->getHead();

    cout << "~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
    cout << "Displaying a list of " << list->getLength() << " item(s):" << endl << endl;

    cout << "[ ";

    while (current != NULL) {
        cout << current->value;
        current = current->next;

        if (current != NULL) {
            cout << ", ";
        }
    }

    cout << " ]" << endl;
    cout << "~~~~~~~~~~~~~~~~~~~~~~~~";
}

/**
 * Starts the demo application.
 */
int main() {
    DoublyLinkedList* list = new DoublyLinkedList();

    list->prepend(50);
    list->prepend(60);
    list->append(70);
    list->insert(10, 2);

    list->sort();

    displayDoublyLinkedList(list);

    return 0;
}